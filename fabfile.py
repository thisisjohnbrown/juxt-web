from fabric.api import *

def live():
	env.hosts = ['clxvi@clxvi.webfactional.com']
	env['dir'] = '/home/pro228b22/webapps/carbon_live/carbon'
	env['apache2'] = '/home/pro228b22/webapps/writing_time/apache2/bin/'
	env['branch'] = 'master'
	env['log'] = 'error_writing_time.log'
	env.password = '138plus28'

def dev():
	env.hosts = ['clxvi@clxvi.webfactional.com']
	env['dir'] = '/home/clxvi/webapps/juxt_dev/juxt'
	env['apache2'] = '/home/clxvi/webapps/juxt_dev/apache2/bin/'
	env['virtenv'] = '/home/clxvi/.virtualenvs/juxt-dev/bin/'
	env['branch'] = 'dev'
	env['log'] = 'error_juxt_dev.log'
	env['environment'] = 'staging'
	env.password = '138plus28'

def pull():
	run ('cd %s; git pull origin %s;' % (env['dir'], env['branch']))

def pip():
	run ('cd %s; %spip install -r requirements.txt;' % (env['dir'], env['virtenv']))

def deploy():
	run ('cd %s; git pull origin %s; npm install; grunt deploy;' % (env['dir'], env['branch']))
	pip()
	static()
	run ('%srestart' % (env['apache2']))

def static():
	run ('cd %s; export DJANGO_SETTINGS_MODULE="juxt.settings.%s"; %spython %s/site/manage.py collectstatic --noinput;' % (env['dir'], env['environment'], env['virtenv'], env['dir']))

def restart():
	run ('cd %s; %srestart' % (env['dir'], env['apache2']))

def log():
	run ('cat /home/clxvi/logs/user/%s' % env['log'])

def rebuild_index():
	run ('cd %s/carbon; python2.7 manage.py rebuild_index' % env['dir'])

def migration():
	run ('cd %s/carbon; python2.7 manage.py migrate' % env['dir'])

def debug_true():
	run ('cd %s; perl -pi -w -e "s/DEBUG = False/DEBUG = True/g;" settings_local.py; /etc/init.d/httpd restart' % env['dir'])

def debug_false():
	run ('cd %s; perl -pi -w -e "s/DEBUG = True/DEBUG = False/g;" settings_local.py; /etc/init.d/httpd restart' % env['dir'])
#!/bin/bash

#command -v virtualenv >/dev/null 2>&1 || { echo >&2 "Please install virtualenv using: \"sudo pip install virtualenv\""; exit 1; }

if [ ! -d env ];
then
    # set up the python environment that's local to the Flipboard install
    echo "Creating the Python virtual environment"
    virtualenv env
fi

env/bin/pip install -r requirements.txt

echo
echo "Complete!"
echo "cd into the site/ directory and run \"../env/bin/python manage.py runserver\" to launch Django"



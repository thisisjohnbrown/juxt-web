from django.contrib import admin
from juxt.models import *

models = [[Pose, PoseAdmin], [UserProfile, 0]]

for model in models:
    admin.site.register(model[0], model[1])


from django.contrib.auth.models import User

from juxt.fields import Base64FileField
from juxt.models import Pose

from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import Authorization
from tastypie.resources import ALL, ModelResource
from tastypie.validation import Validation

class MultipartResource(object):
    def deserialize(self, request, data, format=None):
        print request
        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')

        if format == 'application/x-www-form-urlencoded':
            return request.POST

        if format.startswith('multipart'):
            data = request.POST.copy()
            data.update(request.FILES)
            return data

        return super(MultipartResource, self).deserialize(request, data, format)

    def put_detail(self, request, **kwargs):
        if request.META.get('CONTENT_TYPE').startswith('multipart') and \
                not hasattr(request, '_body'):
            request._body = ''

        return super(MultipartResource, self).put_detail(request, **kwargs)

class CreatorResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser']

class CustomValidation(Validation):
    """
    The custom validation checks two things:
       1) that there is data
       2) that the CompanyId exists (unique check)
    """
    def is_valid(self, bundle, request=None):
        errors = {}                                    
        identifier=bundle.data.get('identifier', None)

        # manager method, returns true if the company exists, false otherwise
        if Pose.objects.filter(identifier=identifier).count():
            errors['identifier']='Duplicate identifier, identifier %s already exists.' % identifier
        return errors

class PoseResource(ModelResource):
    before_image = Base64FileField("before_image")
    after_image = Base64FileField("after_image")
    identifier = fields.CharField('identifier', unique=True)

    class Meta:
        queryset = Pose.objects.all().order_by('-updated_date').filter(deleted=False)
        resource_name = 'pose'
        list_allowed_methods = ['get', 'post']
        filtering = {
            'id': ALL,
            'identifier': ALL
        }
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        validation = CustomValidation()
        # excludes = ['id']
        # always_return_data = True
    def deserialize(self, request, data, format=None):
        return super(PoseResource, self).deserialize(request, data, format)
    def get_object_list(self, request):
        return super(PoseResource, self).get_object_list(request).filter(user__username=request.GET['username'])
    def obj_create(self, bundle, **kwargs):
        return super(PoseResource, self).obj_create(bundle, user=bundle.request.user)
    def apply_authorization_limits(self, request, object_list):
        return object_list.filter(user=request.user)
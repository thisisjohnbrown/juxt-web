import base64
import os
import mimetypes
 
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from tastypie.fields import FileField

from PIL import Image

from StringIO import StringIO
 
class Base64FileField(FileField):
    """
    A django-tastypie field for handling file-uploads through raw post data.
    It uses base64 for en-/decoding the contents of the file.
    Usage:
 
    class MyResource(ModelResource):
        file_field = Base64FileField("file_field")
 
        class Meta:
            queryset = ModelWithFileField.objects.all()
 
    In the case of multipart for submission, it would also pass the filename.
    By using a raw post data stream, we have to pass the filename within our
    file_field structure:
 
    file_field = {
        "name": "myfile.png",
        "file": "longbas64encodedstring",
        "content_type": "image/png" # on hydrate optional
    }
 
    Your file_field will by dehydrated in the above format if the return64
    keyword argument is set to True on the field, otherwise it will simply
    return the URL.
    """
    def convert(self, value):
        if value is None:
            return None

        try:
            # Try to return the URL if it's a ``File``, falling back to the string
            # itself if it's been overridden or is a default.
            return getattr(value, 'url', value)
        except ValueError:
            return None

    def dehydrate(self, bundle, for_list):
        if not bundle.data.has_key(self.instance_name) and hasattr(bundle.obj, self.instance_name):
            file_field = getattr(bundle.obj, self.instance_name)
            if file_field:
                try:
                    content_type, encoding = mimetypes.guess_type(file_field.file.name)
                    b64 = open(file_field.file.name, "rb").read().encode("base64")
                    im = Image.open(StringIO(b64))
                    ret = {
                        "name": os.path.basename(file_field.file.name),
                        "file": b64,
                        "content-type": content_type or "application/octet-stream"
                    }
                    return ret
                except:
                    try:
                        return file_field.url
                    except:
                        pass
        return None
 
    def hydrate(self, obj):
        value = super(FileField, self).hydrate(obj)
        if value:
            try:
                value = SimpleUploadedFile(value["name"], base64.b64decode(value["file"]), getattr(value, "content_type", "application/octet-stream"))
            except:
                pass
        return value
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Pose.before_path'
        db.delete_column('juxt_pose', 'before_path')

        # Adding field 'Pose.caption'
        db.add_column('juxt_pose', 'caption',
                      self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.identifier'
        db.add_column('juxt_pose', 'identifier',
                      self.gf('django.db.models.fields.CharField')(default='asdf', max_length=16),
                      keep_default=False)

        # Adding field 'Pose.uploaded_date'
        db.add_column('juxt_pose', 'uploaded_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 8, 6, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Pose.updated_date'
        db.add_column('juxt_pose', 'updated_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 8, 6, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Pose.swiper_location'
        db.add_column('juxt_pose', 'swiper_location',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.before_image'
        db.add_column('juxt_pose', 'before_image',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.before_offset_x'
        db.add_column('juxt_pose', 'before_offset_x',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.before_offset_y'
        db.add_column('juxt_pose', 'before_offset_y',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.before_scale'
        db.add_column('juxt_pose', 'before_scale',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.after_image'
        db.add_column('juxt_pose', 'after_image',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.after_offset_x'
        db.add_column('juxt_pose', 'after_offset_x',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.after_offset_y'
        db.add_column('juxt_pose', 'after_offset_y',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Pose.after_scale'
        db.add_column('juxt_pose', 'after_scale',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Pose.before_path'
        db.add_column('juxt_pose', 'before_path',
                      self.gf('django.db.models.fields.URLField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Pose.caption'
        db.delete_column('juxt_pose', 'caption')

        # Deleting field 'Pose.identifier'
        db.delete_column('juxt_pose', 'identifier')

        # Deleting field 'Pose.uploaded_date'
        db.delete_column('juxt_pose', 'uploaded_date')

        # Deleting field 'Pose.updated_date'
        db.delete_column('juxt_pose', 'updated_date')

        # Deleting field 'Pose.swiper_location'
        db.delete_column('juxt_pose', 'swiper_location')

        # Deleting field 'Pose.before_image'
        db.delete_column('juxt_pose', 'before_image')

        # Deleting field 'Pose.before_offset_x'
        db.delete_column('juxt_pose', 'before_offset_x')

        # Deleting field 'Pose.before_offset_y'
        db.delete_column('juxt_pose', 'before_offset_y')

        # Deleting field 'Pose.before_scale'
        db.delete_column('juxt_pose', 'before_scale')

        # Deleting field 'Pose.after_image'
        db.delete_column('juxt_pose', 'after_image')

        # Deleting field 'Pose.after_offset_x'
        db.delete_column('juxt_pose', 'after_offset_x')

        # Deleting field 'Pose.after_offset_y'
        db.delete_column('juxt_pose', 'after_offset_y')

        # Deleting field 'Pose.after_scale'
        db.delete_column('juxt_pose', 'after_scale')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'juxt.pose': {
            'Meta': {'object_name': 'Pose'},
            'after_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'after_offset_x': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'after_offset_y': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'after_scale': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'before_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'before_offset_x': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'before_offset_y': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'before_scale': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'swiper_location': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'updated_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'uploaded_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'juxt.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['juxt']
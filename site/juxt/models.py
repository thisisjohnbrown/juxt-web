import os
from cStringIO import StringIO
import urllib

from django.contrib import admin
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import models

from PIL import Image

class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    name = models.CharField(max_length=40)

def create_userprofile(sender, **kwargs):
    """
    A signal for hooking up automatic UserProfile creation.
    """
    if kwargs.get('created') is True:
        UserProfile.objects.create(user=kwargs.get('instance'))

RATIO_CHOICES = (
    (1, 'Foreground Image (1 700px x 610px image)',),
    ('bg-image', 'Background Image (1 1600px x 610px image or wider)',),
    ('cards', 'Cards (2 294px x 432px images)',),
)

class Pose(models.Model):
    SQUARE = 1
    LANDSCAPE = 2
    PORTRAIT= 3
    RATIO_CHOICES = (
        (SQUARE, 'Square'),
        (LANDSCAPE, 'Landscape'),
        (PORTRAIT, 'Portrait'),
    )

    SPLIT_LEFT = 0
    SPLIT_VERTICAL = 1
    SPLIT_RIGHT = 2
    SPLIT_HORIZONTAL = 3
    SPLIT_CHOICES = (
        (SPLIT_LEFT, 'Left'),
        (SPLIT_VERTICAL, 'Vertical'),
        (SPLIT_RIGHT, 'Right'),
        (SPLIT_HORIZONTAL, 'Horizontal'),
    )

    user = models.ForeignKey(User)
    name = models.CharField(max_length=100, blank=True, null=True)
    caption = models.CharField(max_length=250, blank=True, null=True)
    identifier = models.CharField(max_length=16)
    ratio = models.IntegerField(choices=RATIO_CHOICES, default=SQUARE)
    split = models.IntegerField(choices=SPLIT_CHOICES, default=SPLIT_LEFT)

    uploaded_date = models.DateTimeField(auto_now_add=True, editable=True)
    updated_date = models.DateTimeField(auto_now=True, editable=True)
    swiper_location = models.FloatField(blank=True, null=True)

    before_image = models.FileField(upload_to='photos', blank=True, null=True)
    before_image_crop = models.FileField(upload_to='photos', blank=True, null=True)
    before_offset_x = models.FloatField(blank=True, null=True)
    before_offset_y = models.FloatField(blank=True, null=True)
    before_scale = models.FloatField(blank=True, null=True)

    after_image = models.FileField(upload_to='photos', blank=True, null=True)
    after_image_crop = models.FileField(upload_to='photos', blank=True, null=True)
    after_offset_x = models.FloatField(blank=True, null=True)
    after_offset_y = models.FloatField(blank=True, null=True)
    after_scale = models.FloatField(blank=True, null=True)

    deleted = models.BooleanField()
    shared = models.BooleanField()

class PoseAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'uploaded_date', 'updated_date', 'before_image', 'after_image')

def create_thumbnail(sender, **kwargs):
    """
    A signal for automatic thumbnail creation.
    """
    print 'create_thumbnail'
    pose = kwargs.get('instance')
    print pose
    print pose.before_image.url
    if pose.shared and not pose.deleted:
        before_img = create_resized_image(pose.before_image.url,pose.before_offset_x, pose.before_offset_y, pose.before_scale, pose.ratio)
        before_handle = StringIO()
        before_img.save(before_handle, 'jpeg')
        before_handle.seek(0)

        before_file = SimpleUploadedFile(os.path.split(pose.before_image.name)[-1].split('.')[0],
                                     before_handle.read(), content_type='image/jpeg')

        after_img = create_resized_image(pose.after_image.url,pose.after_offset_x, pose.after_offset_y, pose.after_scale, pose.ratio)
        after_handle = StringIO()
        after_img.save(after_handle, 'jpeg')
        after_handle.seek(0)

        after_file = SimpleUploadedFile(os.path.split(pose.after_image.name)[-1].split('.')[0],
                                     after_handle.read(), content_type='image/jpeg')
        # img.save(before_path, 'JPEG')
        # after_path = 'media/tmp/%s-ba.jpg' % pose.identifier
        # img = create_resized_image(pose.after_image.url,pose.after_offset_x, pose.after_offset_y, pose.after_scale, pose.ratio)
        # img.save(after_path, 'JPEG')

        # pose.after_image_crop = after_path

        models.signals.post_save.disconnect(create_thumbnail, sender=Pose)
        pose.before_image_crop.save('%s-bc.jpg' % pose.identifier, before_file, save=True)
        pose.after_image_crop.save('%s-ac.jpg' % pose.identifier, after_file, save=True)
        models.signals.post_save.connect(create_thumbnail, sender=Pose)

def create_resized_image(url, off_x, off_y, scale, ratio):
    file = StringIO(urllib.urlopen(url).read())
    img = Image.open(file)
    w, h = img.size
    r = 1
    if ratio == 2:
        r = 3/4.0
    elif ratio == 3:
        r = 4/3.0
    start_x = (int)(w*off_x*scale)
    start_y = (int)(h*off_y*scale*(float(w)/h))
    width = (int)(w*scale)+start_x
    height = (int)(w*scale*r)+start_y
    print '-----------'
    print float(w)/h
    print off_y
    print scale
    print start_y
    print height
    return img.crop((start_x, start_y, width, height))

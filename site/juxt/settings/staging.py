from defaults import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'juxt_dev',                      
        'USER': 'juxt_user',                      
        'PASSWORD': 'f1ve5Hedgehogs',                  
        'HOST': 'web226.webfaction.com',                      
        'PORT': '5432',                      
    }
}

# Storages
AWS_STORAGE_BUCKET_NAME = 'juxt-staging'
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

S3_URL = 'http://s3.amazonaws.com/%s' % AWS_STORAGE_BUCKET_NAME
#STATIC_URL = S3_URL + STATIC_ROOT
MEDIA_URL = S3_URL + MEDIA_ROOT

STATIC_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME

DJANGO_SETTINGS_MODULE = 'juxt.settings.staging'

import juxt.signals
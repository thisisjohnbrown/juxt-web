from defaults import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'worb.db'
    }
}

SOUTH_TESTS_MIGRATE = False

from django.contrib.auth.models import User
from django.db import models

from juxt.models import create_thumbnail, create_userprofile, Pose

from tastypie.models import create_api_key

models.signals.post_save.connect(create_api_key, sender=User)
models.signals.post_save.connect(create_userprofile, sender=User)

models.signals.post_save.connect(create_thumbnail, sender=Pose)
var split;
var canvas;
var ctx;
var before;
var after;
var ratio;
var canvasPerc;
var container;
var image_before = document.createElement('IMG');
var image_after = document.createElement('IMG');

window.onload = function() {
    canvas = document.getElementById('c');
    container = document.getElementsByClassName('container')[0];
    var r = parseInt(container.attributes.ratio.value);
    canvasPerc = 1;
    if (r == 2) {
        canvasPerc = 1.33333;
    } else if (r == 3) {
        canvasPerc = .75;
    }
    ctx = canvas.getContext('2d');
    split = parseInt(canvas.attributes.split.value);
    before = canvas.attributes.before.value;
    after = canvas.attributes.after.value;
    image_after.src = before.substr(0, before.indexOf('?')+1);
}

window.onresize = function() {
    resetCanvas();
}

image_before.onload = function() {
    resetCanvas();
    canvas.onmousemove = canvasMove;
    canvas.onmouseout = canvasOut;
}

image_after.onload = function () {
    image_before.src = after.substr(0, after.indexOf('?')+1);
}

function resetCanvas() {
    container.style.height = container.offsetWidth * canvasPerc + "px";
    canvas.height = container.offsetWidth * (canvasPerc);
    canvas.width = container.offsetWidth;
    drawMask(canvas.width/2, canvas.height/2);
}

function canvasMove(e) {
    x=e.clientX - canvas.offsetLeft;
    y=e.clientY - canvas.offsetTop;
    ctx.clearRect (0, 0, canvas.width, canvas.height );
    drawMask(x, y);
}

function canvasOut(e) {
    drawMask(canvas.width/2, canvas.height/2);
}

function drawMask(x, y) {
    ctx.save();
    cW = canvas.width;
    cH = canvas.height;
    console.log(cW, cH);
    var ratio = cW/cH;
    ctx.drawImage(image_after, 0, 0, cW, cH);
    ctx.beginPath();
    
    var perc;
    if (split == 3) {
        perc = y/cH;
        ctx.moveTo(0, cH*perc);
        ctx.lineTo(cW, cH*perc);
        ctx.lineTo(cW, cH);
        ctx.lineTo(0, cH);
    } else if (split == 1) {
        perc = x/cW;
        ctx.moveTo(cW*perc, 0);
        ctx.lineTo(cW, 0);
        ctx.lineTo(cW, cH);
        ctx.lineTo(cW*perc, cH);
    } else if (split == 0) {
        perc = ((x-(y*ratio))/cW + 1) / 2;
        var offset = cW-(cW*(2-(perc*2)));
        ctx.moveTo(offset, 0);
        ctx.lineTo(offset + cW, cH);
        ctx.lineTo(offset + cW*2, cH);
        ctx.lineTo(offset + cW*2, 0);
    } else if (split == 2) {
        perc = ((x+(y*ratio))/cW) / 2;
        var offset = cW*(perc*2);
        ctx.moveTo(offset, 0);
        ctx.lineTo(offset + cW, 0);
        ctx.lineTo(offset + cW, cH);
        ctx.lineTo(offset - cW, cH);
    }
    
    ctx.closePath();
    ctx.clip();
    ctx.drawImage(image_before, 0, 0, cW, cH);
 
    ctx.restore();
}
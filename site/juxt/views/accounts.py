import simplejson

from django.contrib.auth.models import User
from django.http import Http404, HttpResponse

from tastypie.models import ApiKey

def create(request):
	error = []
	try:
		request.GET['email']
		request.GET['username']
		request.GET['password']
	except:
		error = 'missing'
	if not error:
		try:
			User.objects.get(email=request.GET['email'])
			error.append('email')
		except:
			pass
		try:
			User.objects.get(username=request.GET['username'])
			error.append('username')
		except:
			pass
	if error:
		results = {
			'status': 'error',
			'message': error
		}
	else:
		user = User.objects.create_user(request.GET['username'], request.GET['email'], request.GET['password'])
		profile = user.get_profile()
		profile.name = request.GET['display']
		print request.GET['display'];
		profile.save()
		api_key = ApiKey.objects.get_or_create(user=user)
		results = {
			'status': 'success',
			'api-key': api_key[0].key
		}
	return HttpResponse(simplejson.dumps(results), content_type='application/json')

def login(request):
	error = []
	user = 0
	valid = False
	try:
		request.GET['username']
		request.GET['password']
	except:
		error.append('missing')
	if not error:
		try:
			print 'aaa'
			user = User.objects.get(username=request.GET['username'])
			print 'bbb'
			api_key = ApiKey.objects.get_or_create(user=user)
			print 'ccc'
		except:
			error.append('username')

	if not error:
		valid = user.check_password(request.GET['password'])

	if not valid and not error:
		error.append('wrong password')

	if error:
		results = {
			'status': 'error',
			'message': error
		}
	else:
		results = {
			'status': 'success',
			'api-key': api_key[0].key
		}
	return HttpResponse(simplejson.dumps(results), content_type='application/json')
import simplejson

from django.conf import settings
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext

from tastypie.models import ApiKey

def index(request):
	return render_to_response('index.html', locals(), context_instance=RequestContext(request))
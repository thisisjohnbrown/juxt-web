import simplejson

from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext

from juxt.models import Pose

def main(request, id):
	pose = get_object_or_404(Pose, identifier=id, shared=True, deleted=False)
	return render_to_response('pose.html', locals(), context_instance=RequestContext(request))
#!/usr/bin/env python
import os
import sys

from django.core.management import execute_from_command_line

if __name__ == "__main__":

    # we default to local configuration.
    # to override this, pass '--settings flipboard.settings.[ENV]' to manage.py
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "juxt.settings.local")
    execute_from_command_line(sys.argv)

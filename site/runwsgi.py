import os, sys, site
import newrelic.agent
from django.core.wsgi import get_wsgi_application

# Tell wsgi to add the Python site-packages to its path. 
site.addsitedir('/home/clxvi/.virtualenvs/juxt-dev/lib/python2.7/site-packages')

activate_this = os.path.expanduser("~/.virtualenvs/juxt-dev/bin/activate_this.py")
execfile(activate_this, dict(__file__=activate_this))

# Calculate the path based on the location of the WSGI script

full_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(full_path)

config_file = os.environ.get('NEW_RELIC_CONFIG_FILE')
environment = os.environ.get('NEW_RELIC_ENVIRONMENT')

newrelic.agent.initialize(config_file, environment)

os.environ['DJANGO_SETTINGS_MODULE'] = 'juxt.settings.staging'

application = get_wsgi_application()
application = newrelic.agent.wsgi_application()(application)
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *
from django.conf.urls.static import static
from django.contrib import admin

from juxt.api import PoseResource, CreatorResource

from tastypie.api import Api

admin.autodiscover()
v1_api = Api(api_name='v1')
v1_api.register(PoseResource())
v1_api.register(CreatorResource())

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^account/create/?', 'juxt.views.accounts.create'),
    url(r'^account/login/?', 'juxt.views.accounts.login'),

    (r'^api/', include(v1_api.urls)),

    url(r'^(?P<id>\w{4})/?', 'juxt.views.poses.main'),
    url(r'^', 'juxt.views.pages.index'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)